﻿using Core.Data.Endomondo.Gpx;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace Core.Test
{
    [TestClass]
    public class MergerTest
    {
        #region AddExtensionToPoint
        [TestMethod]
        public void AddExtensionToPointTest()
        {
            var point = new Trkpt();
            var hrData = new List<KeyValuePair<DateTime, int>>(1);
            var dateTime = DateTime.Now;
            int hrValue = 100;
            hrData.Add(new KeyValuePair<DateTime, int>(dateTime, hrValue));

            Merger.AddExtensionToPoint(point, hrData);
            Assert.IsNotNull(point);
            Assert.IsNotNull(point.extensions);
            Assert.IsNotNull(point.extensions.TrackPointExtension);
            Assert.AreEqual<int>(point.extensions.TrackPointExtension.hr, hrValue);
        }

        [TestMethod]
        public void AddExtensionToPointAverageTest()
        {
            var point = new Trkpt();
            var hrData = new List<KeyValuePair<DateTime, int>>(3);
            var dateTime = DateTime.Now;
            int hrValue = 100;
            int hrValue1 = 110;
            int hrValue2 = 120;
            hrData.Add(new KeyValuePair<DateTime, int>(dateTime, hrValue));
            hrData.Add(new KeyValuePair<DateTime, int>(dateTime, hrValue1));
            hrData.Add(new KeyValuePair<DateTime, int>(dateTime, hrValue2));

            Merger.AddExtensionToPoint(point, hrData);
            Assert.IsNotNull(point);
            Assert.IsNotNull(point.extensions);
            Assert.IsNotNull(point.extensions.TrackPointExtension);
            Assert.AreEqual<int>(point.extensions.TrackPointExtension.hr, hrValue1);
        }

        [TestMethod]
        public void AddExtensionToPointNullTest()
        {
            var point = new Trkpt();
            Merger.AddExtensionToPoint(point, null);
            Assert.IsNotNull(point);
            Assert.IsNull(point.extensions);
        }

        [TestMethod]
        public void AddExtensionToPointEmptyTest()
        {
            var point = new Trkpt();
            var hrData = new List<KeyValuePair<DateTime, int>>();

            Merger.AddExtensionToPoint(point, hrData);
            Assert.IsNotNull(point);
            Assert.IsNull(point.extensions);
        }
        #endregion //AddExtensionToPoint
    }
}
