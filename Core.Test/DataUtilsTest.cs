﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Core.Test
{
    [TestClass]
    public class DataUtilsTest
    {
        #region GetIntFromString
        [TestMethod]
        public void GetIntFromStringTest()
        {
            string value = "42";
            var result = DataUtils.GetIntFromString(value);
            Assert.AreEqual(value, result.ToString());
        }

        [TestMethod]
        public void GetIntFromStringNullTest()
        {
            var result = DataUtils.GetIntFromString(null);
            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void GetIntFromStringEmptyTest()
        {
            string value = string.Empty;
            var result = DataUtils.GetIntFromString(null);
            Assert.AreEqual<int>(0, result);
        }
        #endregion //GetIntFromString

        #region GetIntArrayFromString
        [TestMethod]
        public void GetIntArrayFromStringTest()
        {
            string values = "101,103,98,99,101,100,102,105,101,100,104,109,114,118,122,126,130";
            var result = DataUtils.GetIntArrayFromString(values);
            var valuesTest = string.Join<int>(",", result);
            Assert.AreEqual<string>(values, valuesTest);
        }

        [TestMethod]
        public void GetIntArrayFromStringNullTest()
        {
            var result = DataUtils.GetIntArrayFromString(null);
            Assert.IsNull(result);
        }

        [TestMethod]
        public void GetIntArrayFromStringEmptyTest()
        {
            string values = string.Empty;
            var result = DataUtils.GetIntArrayFromString(values);
            Assert.IsNull(result);
        }
        #endregion //GetIntArrayFromString

        #region GetDateTimeFromString
        [TestMethod]
        public void GetDateTimeFromStringTest()
        {
            string dateTime = "2014-11-30 08:44:22.0";
            var result = DataUtils.GetDateTimeFromString(dateTime);
            Assert.IsTrue(result.Year == 2014);
            Assert.IsTrue(result.Month == 11);
            Assert.IsTrue(result.Day == 30);
            Assert.IsTrue(result.Hour == 8);
            Assert.IsTrue(result.Minute == 44);
            Assert.IsTrue(result.Second == 22);
        }

        [TestMethod]
        public void GetDateTimeFromStringNullTest()
        {
            var result = DataUtils.GetDateTimeFromString(null);
            Assert.AreEqual(result, DateTime.MinValue);
        }
        #endregion //GetDateTimeFromString

        #region Between
        [TestMethod]
        public void BetweenDateTimeTrueTest()
        {
            var input = DateTime.Now;
            var first = input.AddHours(-1);
            var second = input.AddHours(1);

            var result = DataUtils.Between(input, first, second);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void BetweenDateTimeFalseTest()
        {
            var input = DateTime.Now;
            var first = input.AddHours(1);
            var second = input.AddHours(2);

            var result = DataUtils.Between(input, first, second);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void BetweenDateTimeEqualTest()
        {
            var input = DateTime.Now;
            var first = input;
            var second = input;

            var result = DataUtils.Between(input, first, second);
            Assert.IsTrue(result);
        }
        #endregion //Between
    }
}
