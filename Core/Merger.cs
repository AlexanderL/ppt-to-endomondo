﻿using Core.Data.Endomondo.Gpx;
using Core.Data.Ppt.Xml;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Core
{
    public static class Merger
    {
        /// <summary>
        /// Method to merge training data from endomondo.com with HR data from polarpersonaltrainer.com
        /// </summary>
        /// <param name="endomondoData">Data from endomondo.com</param>
        /// <param name="polarData">Data from polarpersonaltrainer.com</param>
        /// <param name="endomondoTimeShift">Time shift for endomondo's data</param>
        public static void Merge(gpx endomondoData, polarexercisedata polarData, int endomondoTimeShift = 0)
        {
            int sergentsCount = endomondoData.trk.trkseg.Count;
            for (int i = 0; i < sergentsCount; i++)
            {
                int pointsCount = endomondoData.trk.trkseg[i].trkpt.Count;
                for (int j = 0; j < pointsCount; j++)
                {
                    Trkpt point = endomondoData.trk.trkseg[i].trkpt[j];
                    Trkpt nextPoint = null;
                    if (j + 1 < pointsCount)
                        nextPoint = endomondoData.trk.trkseg[i].trkpt[j + 1];

                    IEnumerable<KeyValuePair<DateTime, int>> hrData = null;
                    if (nextPoint != null)
                        hrData = GetSamplesBetweenTwoPoints(polarData.calendaritems.exercise.result.samples.sample,
                            point.time.AddHours(endomondoTimeShift),
                            nextPoint.time.AddHours(endomondoTimeShift));
                    else
                        hrData = GetSampleRightAfterPoint(polarData.calendaritems.exercise.result.samples.sample,
                            point.time.AddHours(endomondoTimeShift));

                    AddExtensionToPoint(point, hrData);
                }
            }
        }

        /// <summary>
        /// Method to add average HR data to point
        /// </summary>
        /// <param name="point">Point</param>
        /// <param name="hrData">Array of HR data values</param>
        internal static void AddExtensionToPoint(Trkpt point, IEnumerable<KeyValuePair<DateTime, int>> hrData)
        {
            if (hrData == null || hrData.Count() == 0)
                return;

            var avgHrValue =
                (from hr in hrData
                 select hr.Value).Average();

            point.extensions = new TrkptExtensions { TrackPointExtension = new TrackPointExtension { hr = Convert.ToByte(avgHrValue) } };
        }

        /// <summary>
        /// Method returns all samples between two dates
        /// </summary>
        /// <param name="sample">Variable that contains all the samples</param>
        /// <param name="firstPoint">First date</param>
        /// <param name="secondPoint">Second date</param>
        /// <returns></returns>
        internal static IEnumerable<KeyValuePair<DateTime, int>> GetSamplesBetweenTwoPoints(Sample sample, DateTime firstPoint, DateTime secondPoint)
        {
            return sample.SampleData.Where(d => DataUtils.Between(d.Key, firstPoint, secondPoint));
        }

        /// <summary>
        /// Method returns first sample after specified date
        /// </summary>
        /// <param name="sample">Variable that contains all the samples</param>
        /// <param name="point">Date</param>
        /// <returns></returns>
        internal static IEnumerable<KeyValuePair<DateTime, int>> GetSampleRightAfterPoint(Sample sample, DateTime point)
        {
            List<KeyValuePair<DateTime, int>> result = new List<KeyValuePair<DateTime, int>>(1);
            result.Add(sample.SampleData.FirstOrDefault(d => d.Key >= point));
            return result;
        }
    }
}
