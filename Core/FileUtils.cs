﻿using System.IO;

namespace Core
{
    public static class FileUtils
    {
        public static string GetFreeOutputFileName(string fileName)
        {
            string result;
            int index = 0;
            do
            {
                result = MakeOutputFileName(fileName, index++);
            } while (File.Exists(result));

            return result;
        }

        public static string MakeOutputFileName(string fileName, int index = 0)
        {
            return string.Format("{0}{1}{2}_merged{3}{4}",
                Path.GetDirectoryName(fileName),
                Path.DirectorySeparatorChar,
                Path.GetFileNameWithoutExtension(fileName),
                index == 0 ? string.Empty : string.Format(" ({0})", index),
                Path.GetExtension(fileName));
        }
    }
}
