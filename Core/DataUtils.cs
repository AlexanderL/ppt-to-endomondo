﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace Core
{
    internal class DataUtils
    {
        internal static DateTime GetDateTimeFromString(string param)
        {
            if (string.IsNullOrEmpty(param))
                return DateTime.MinValue;

            return DateTime.Parse(param);
        }

        internal static IEnumerable<int> GetIntArrayFromString(string param)
        {
            if (string.IsNullOrEmpty(param))
                return null;

            string[] digits = Regex.Split(param, @"\D+");
            return digits.Select(d => int.Parse(d));
        }

        internal static int GetIntFromString(string param)
        {
            if (string.IsNullOrEmpty(param))
                return 0;

            return Convert.ToInt16(param, CultureInfo.InvariantCulture);
        }

        internal static bool Between(IComparable input, IComparable a, IComparable b)
        {
            return input.CompareTo(a) >= 0 && input.CompareTo(b) <= 0;
        }
    }
}
