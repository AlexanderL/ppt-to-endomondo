﻿using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace Core
{
    static class ParseHelpers
    {
        private static Stream ToStream(string text)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(text);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        internal static T DeserializeFromFile<T>(string fileName) where T : class
        {
            string xml = File.ReadAllText(fileName);
            var reader = XmlReader.Create(ToStream(xml.Trim()), new XmlReaderSettings() { ConformanceLevel = ConformanceLevel.Document });
            return new XmlSerializer(typeof(T)).Deserialize(reader) as T;
        }

        internal static void SerializeToFile<T>(string fileName, T obj) where T : class
        {
            using (var writer = XmlWriter.Create(fileName, new XmlWriterSettings() { ConformanceLevel = ConformanceLevel.Document }))
            {
                new XmlSerializer(typeof(T)).Serialize(writer, obj);
                writer.Close();
            }
        }
    }
}
