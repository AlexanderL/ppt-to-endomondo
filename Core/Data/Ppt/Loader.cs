﻿using Core.Data.Ppt.Xml;

namespace Core.Data.Ppt
{
    public class Loader : ILoader<polarexercisedata>
    {
        public polarexercisedata LoadDataFromFile(string filePath)
        {
            var result = ParseHelpers.DeserializeFromFile<polarexercisedata>(filePath);

            var startTile = DataUtils.GetDateTimeFromString(result.calendaritems.exercise.time);
            var interval = result.calendaritems.exercise.result.recordingrate;
            result.calendaritems.exercise.result.samples.sample.FillSampleData(startTile, interval);

            return result;
        }
    }
}