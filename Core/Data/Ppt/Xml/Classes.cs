﻿using System.Collections.Generic;

namespace Core.Data.Ppt.Xml
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.polarpersonaltrainer.com")]
    [System.Xml.Serialization.XmlRootAttribute("polar-exercise-data", Namespace = "http://www.polarpersonaltrainer.com", IsNullable = false)]
    public partial class polarexercisedata
    {

        private CalendarItems calendaritemsField;

        private decimal versionField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("calendar-items")]
        public CalendarItems calendaritems
        {
            get
            {
                return this.calendaritemsField;
            }
            set
            {
                this.calendaritemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal version
        {
            get
            {
                return this.versionField;
            }
            set
            {
                this.versionField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.polarpersonaltrainer.com")]
    public partial class CalendarItems
    {

        private Exercise exerciseField;

        private byte countField;

        /// <remarks/>
        public Exercise exercise
        {
            get
            {
                return this.exerciseField;
            }
            set
            {
                this.exerciseField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte count
        {
            get
            {
                return this.countField;
            }
            set
            {
                this.countField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.polarpersonaltrainer.com")]
    public partial class Exercise
    {

        private string createdField;

        private string timeField;

        private ExerciseResult resultField;

        /// <remarks/>
        public string created
        {
            get
            {
                return this.createdField;
            }
            set
            {
                this.createdField = value;
            }
        }

        /// <remarks/>
        public string time
        {
            get
            {
                return this.timeField;
            }
            set
            {
                this.timeField = value;
            }
        }

        /// <remarks/>
        public ExerciseResult result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.polarpersonaltrainer.com")]
    public partial class ExerciseResult
    {

        private ushort caloriesField;

        private System.DateTime durationField;

        private UserSettings usersettingsField;

        private List<ResultZone> zonesField;

        private Heartrate heartrateField;

        private byte recordingrateField;

        private byte fatconsumptionField;

        private Speed speedField;

        private Samples samplesField;

        /// <remarks/>
        public ushort calories
        {
            get
            {
                return this.caloriesField;
            }
            set
            {
                this.caloriesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "time")]
        public System.DateTime duration
        {
            get
            {
                return this.durationField;
            }
            set
            {
                this.durationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("user-settings")]
        public UserSettings usersettings
        {
            get
            {
                return this.usersettingsField;
            }
            set
            {
                this.usersettingsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("zone", IsNullable = false)]
        public List<ResultZone> zones
        {
            get
            {
                return this.zonesField;
            }
            set
            {
                this.zonesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("heart-rate")]
        public Heartrate heartrate
        {
            get
            {
                return this.heartrateField;
            }
            set
            {
                this.heartrateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("recording-rate")]
        public byte recordingrate
        {
            get
            {
                return this.recordingrateField;
            }
            set
            {
                this.recordingrateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("fat-consumption")]
        public byte fatconsumption
        {
            get
            {
                return this.fatconsumptionField;
            }
            set
            {
                this.fatconsumptionField = value;
            }
        }

        /// <remarks/>
        public Speed speed
        {
            get
            {
                return this.speedField;
            }
            set
            {
                this.speedField = value;
            }
        }

        /// <remarks/>
        public Samples samples
        {
            get
            {
                return this.samplesField;
            }
            set
            {
                this.samplesField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.polarpersonaltrainer.com")]
    public partial class UserSettings
    {

        private UserSettingsHeartrate heartrateField;

        private byte vo2maxField;

        private decimal weightField;

        private decimal heightField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("heart-rate")]
        public UserSettingsHeartrate heartrate
        {
            get
            {
                return this.heartrateField;
            }
            set
            {
                this.heartrateField = value;
            }
        }

        /// <remarks/>
        public byte vo2max
        {
            get
            {
                return this.vo2maxField;
            }
            set
            {
                this.vo2maxField = value;
            }
        }

        /// <remarks/>
        public decimal weight
        {
            get
            {
                return this.weightField;
            }
            set
            {
                this.weightField = value;
            }
        }

        /// <remarks/>
        public decimal height
        {
            get
            {
                return this.heightField;
            }
            set
            {
                this.heightField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.polarpersonaltrainer.com")]
    public partial class UserSettingsHeartrate
    {

        private byte restingField;

        private byte maximumField;

        /// <remarks/>
        public byte resting
        {
            get
            {
                return this.restingField;
            }
            set
            {
                this.restingField = value;
            }
        }

        /// <remarks/>
        public byte maximum
        {
            get
            {
                return this.maximumField;
            }
            set
            {
                this.maximumField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.polarpersonaltrainer.com")]
    public partial class ResultZone
    {

        private byte upperField;

        private byte lowerField;

        private System.DateTime inzoneField;

        private byte indexField;

        /// <remarks/>
        public byte upper
        {
            get
            {
                return this.upperField;
            }
            set
            {
                this.upperField = value;
            }
        }

        /// <remarks/>
        public byte lower
        {
            get
            {
                return this.lowerField;
            }
            set
            {
                this.lowerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("in-zone", DataType = "time")]
        public System.DateTime inzone
        {
            get
            {
                return this.inzoneField;
            }
            set
            {
                this.inzoneField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte index
        {
            get
            {
                return this.indexField;
            }
            set
            {
                this.indexField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.polarpersonaltrainer.com")]
    public partial class Heartrate
    {

        private byte averageField;

        private byte maximumField;

        /// <remarks/>
        public byte average
        {
            get
            {
                return this.averageField;
            }
            set
            {
                this.averageField = value;
            }
        }

        /// <remarks/>
        public byte maximum
        {
            get
            {
                return this.maximumField;
            }
            set
            {
                this.maximumField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.polarpersonaltrainer.com")]
    public partial class Speed
    {

        private string typeField;

        /// <remarks/>
        public string type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.polarpersonaltrainer.com")]
    public partial class Samples
    {

        private Sample sampleField;

        /// <remarks/>
        public Sample sample
        {
            get
            {
                return this.sampleField;
            }
            set
            {
                this.sampleField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.polarpersonaltrainer.com")]
    public partial class Sample
    {

        private string typeField;

        private string valuesField;

        /// <remarks/>
        public string type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        public string values
        {
            get
            {
                return this.valuesField;
            }
            set
            {
                this.valuesField = value;
            }
        }
    }


}
