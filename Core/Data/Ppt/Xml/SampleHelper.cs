﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace Core.Data.Ppt.Xml
{
    public partial class Sample
    {
        [XmlIgnore()]
        public Dictionary<DateTime, int> SampleData { get; set; }

        internal void FillSampleData(DateTime startTime, byte interval)
        {
            var hrValues = DataUtils.GetIntArrayFromString(values);
            SampleData = new Dictionary<DateTime, int>(hrValues.Count());
            foreach (var hr in hrValues)
            {
                SampleData.Add(startTime, hr);
                startTime = startTime.AddSeconds(interval);
            }
        }

        
    }
}
