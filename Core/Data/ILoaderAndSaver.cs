﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Data
{
    public interface ILoaderAndSaver<T> : ILoader<T>
    {
        void SaveDataToFile(T data, string fileName);
    }
}
