﻿using System.Collections.Generic;

namespace Core.Data.Endomondo.Tcx
{

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2", IsNullable = false)]
    public partial class TrainingCenterDatabase
    {

        private TrainingCenterDatabaseActivities activitiesField;

        /// <remarks/>
        public TrainingCenterDatabaseActivities Activities
        {
            get
            {
                return this.activitiesField;
            }
            set
            {
                this.activitiesField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2")]
    public partial class TrainingCenterDatabaseActivities
    {

        private TrainingCenterDatabaseActivitiesActivity activityField;

        /// <remarks/>
        public TrainingCenterDatabaseActivitiesActivity Activity
        {
            get
            {
                return this.activityField;
            }
            set
            {
                this.activityField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2")]
    public partial class TrainingCenterDatabaseActivitiesActivity
    {

        private System.DateTime idField;

        private TrainingCenterDatabaseActivitiesActivityLap lapField;

        private string sportField;

        /// <remarks/>
        public System.DateTime Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public TrainingCenterDatabaseActivitiesActivityLap Lap
        {
            get
            {
                return this.lapField;
            }
            set
            {
                this.lapField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Sport
        {
            get
            {
                return this.sportField;
            }
            set
            {
                this.sportField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2")]
    public partial class TrainingCenterDatabaseActivitiesActivityLap
    {

        private decimal totalTimeSecondsField;

        private decimal distanceMetersField;

        private ushort caloriesField;

        private TrainingCenterDatabaseActivitiesActivityLapAverageHeartRateBpm averageHeartRateBpmField;

        private TrainingCenterDatabaseActivitiesActivityLapMaximumHeartRateBpm maximumHeartRateBpmField;

        private string intensityField;

        private string triggerMethodField;

        private List<TrainingCenterDatabaseActivitiesActivityLapTrackpoint> trackField;

        private System.DateTime startTimeField;

        /// <remarks/>
        public decimal TotalTimeSeconds
        {
            get
            {
                return this.totalTimeSecondsField;
            }
            set
            {
                this.totalTimeSecondsField = value;
            }
        }

        /// <remarks/>
        public decimal DistanceMeters
        {
            get
            {
                return this.distanceMetersField;
            }
            set
            {
                this.distanceMetersField = value;
            }
        }

        /// <remarks/>
        public ushort Calories
        {
            get
            {
                return this.caloriesField;
            }
            set
            {
                this.caloriesField = value;
            }
        }

        /// <remarks/>
        public TrainingCenterDatabaseActivitiesActivityLapAverageHeartRateBpm AverageHeartRateBpm
        {
            get
            {
                return this.averageHeartRateBpmField;
            }
            set
            {
                this.averageHeartRateBpmField = value;
            }
        }

        /// <remarks/>
        public TrainingCenterDatabaseActivitiesActivityLapMaximumHeartRateBpm MaximumHeartRateBpm
        {
            get
            {
                return this.maximumHeartRateBpmField;
            }
            set
            {
                this.maximumHeartRateBpmField = value;
            }
        }

        /// <remarks/>
        public string Intensity
        {
            get
            {
                return this.intensityField;
            }
            set
            {
                this.intensityField = value;
            }
        }

        /// <remarks/>
        public string TriggerMethod
        {
            get
            {
                return this.triggerMethodField;
            }
            set
            {
                this.triggerMethodField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Trackpoint", IsNullable = false)]
        public List<TrainingCenterDatabaseActivitiesActivityLapTrackpoint> Track
        {
            get
            {
                return this.trackField;
            }
            set
            {
                this.trackField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime StartTime
        {
            get
            {
                return this.startTimeField;
            }
            set
            {
                this.startTimeField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2")]
    public partial class TrainingCenterDatabaseActivitiesActivityLapAverageHeartRateBpm
    {

        private byte valueField;

        /// <remarks/>
        public byte Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2")]
    public partial class TrainingCenterDatabaseActivitiesActivityLapMaximumHeartRateBpm
    {

        private byte valueField;

        /// <remarks/>
        public byte Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2")]
    public partial class TrainingCenterDatabaseActivitiesActivityLapTrackpoint
    {

        private System.DateTime timeField;

        private TrainingCenterDatabaseActivitiesActivityLapTrackpointPosition positionField;

        private decimal altitudeMetersField;

        private bool altitudeMetersFieldSpecified;

        private TrainingCenterDatabaseActivitiesActivityLapTrackpointHeartRateBpm heartRateBpmField;

        /// <remarks/>
        public System.DateTime Time
        {
            get
            {
                return this.timeField;
            }
            set
            {
                this.timeField = value;
            }
        }

        /// <remarks/>
        public TrainingCenterDatabaseActivitiesActivityLapTrackpointPosition Position
        {
            get
            {
                return this.positionField;
            }
            set
            {
                this.positionField = value;
            }
        }

        /// <remarks/>
        public decimal AltitudeMeters
        {
            get
            {
                return this.altitudeMetersField;
            }
            set
            {
                this.altitudeMetersField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AltitudeMetersSpecified
        {
            get
            {
                return this.altitudeMetersFieldSpecified;
            }
            set
            {
                this.altitudeMetersFieldSpecified = value;
            }
        }

        /// <remarks/>
        public TrainingCenterDatabaseActivitiesActivityLapTrackpointHeartRateBpm HeartRateBpm
        {
            get
            {
                return this.heartRateBpmField;
            }
            set
            {
                this.heartRateBpmField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2")]
    public partial class TrainingCenterDatabaseActivitiesActivityLapTrackpointPosition
    {

        private decimal latitudeDegreesField;

        private decimal longitudeDegreesField;

        /// <remarks/>
        public decimal LatitudeDegrees
        {
            get
            {
                return this.latitudeDegreesField;
            }
            set
            {
                this.latitudeDegreesField = value;
            }
        }

        /// <remarks/>
        public decimal LongitudeDegrees
        {
            get
            {
                return this.longitudeDegreesField;
            }
            set
            {
                this.longitudeDegreesField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2")]
    public partial class TrainingCenterDatabaseActivitiesActivityLapTrackpointHeartRateBpm
    {

        private byte valueField;

        /// <remarks/>
        public byte Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }


}
