﻿using Core.Data.Endomondo.Gpx;

namespace Core.Data.Endomondo
{
    public class Loader : ILoaderAndSaver<gpx>
    {
        public gpx LoadDataFromFile(string filePath)
        {
            return ParseHelpers.DeserializeFromFile<gpx>(filePath);
        }

        public void SaveDataToFile(gpx data, string fileName)
        {
            ParseHelpers.SerializeToFile<gpx>(fileName, data);
        }
    }
}
