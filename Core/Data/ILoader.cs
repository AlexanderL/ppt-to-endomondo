﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Data
{
    public interface ILoader<T>
    {
        T LoadDataFromFile(string filePath);
    }
}
