﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PptToEndomondo.Design;
using PptToEndomondo.Model;
using PptToEndomondo.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PptToEndomondo.Test.ViewModels
{
    [TestClass]
    public class MainWindowViewModelTest
    {
        private string gpxFilePath = "tcxFilePath";
        private string xmlFilePath = "xmlFilePath";
        private IClientData clientData;

        [TestInitialize]
        public void Init()
        {
            clientData = new DesignClientData();
        }

        [TestMethod]
        public void CreateViewModel()
        {
            var viewModel = new MainWindowViewModel(clientData);
            Assert.IsNotNull(viewModel.ClientData);
        }
        
        [TestMethod]
        public void CreateAndSetTcxFilePath()
        {
            var viewModel = new MainWindowViewModel(clientData);
            viewModel.ClientData.GpxFilePath = gpxFilePath;
            Assert.AreEqual(viewModel.ClientData.GpxFilePath, gpxFilePath);
        }

        [TestMethod]
        public void CreateAndSetXmlFilePath()
        {
            var viewModel = new MainWindowViewModel(clientData);
            viewModel.ClientData.XmlFilePath = xmlFilePath;
            Assert.AreEqual(viewModel.ClientData.XmlFilePath, xmlFilePath);
        }

        [TestMethod]
        public void ConvertCommandWithOnlyTcxTest()
        {
            var viewModel = new MainWindowViewModel(clientData);
            viewModel.ClientData.GpxFilePath = gpxFilePath;
            Assert.IsFalse(viewModel.MergeCommand.CanExecute(null));
        }

        [TestMethod]
        public void ConvertCommandWithOnlyXmlTest()
        {
            var viewModel = new MainWindowViewModel(clientData);
            viewModel.ClientData.XmlFilePath = xmlFilePath;
            Assert.IsFalse(viewModel.MergeCommand.CanExecute(null));
        }

        [TestMethod]
        public void ConvertCommandWithXmlAndTcxTest()
        {
            var viewModel = new MainWindowViewModel(clientData);
            viewModel.ClientData.XmlFilePath = xmlFilePath;
            viewModel.ClientData.GpxFilePath = gpxFilePath;
            Assert.IsTrue(viewModel.MergeCommand.CanExecute(null));
        }
    }
}
