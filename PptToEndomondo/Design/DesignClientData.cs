﻿using Core;
using Core.Data.Endomondo.Gpx;
using Core.Data.Ppt.Xml;
using GalaSoft.MvvmLight;
using PptToEndomondo.Model;
using System;
using System.IO;
using System.Threading.Tasks;

namespace PptToEndomondo.Design
{
    public class DesignClientData : ObservableObject, IClientData
    {
        public DesignClientData()
        {
            GpxFilePath = "Path to GPX file here";
            XmlFilePath = "Path to XML file here";
            Offset = 2;
            EndomondoTrainingStartTime = DateTime.Now.AddDays(-1).ToShortTimeString();
            PptTrainingStartTime = DateTime.Now.AddDays(-1).AddHours(2).ToShortTimeString();
        }

        public bool IsReady
        {
            get { return true; }
        }

        private string gpxFilePath;
        public string GpxFilePath
        {
            get { return gpxFilePath; }
            set
            {
                Set<string>(ref gpxFilePath, value);
            }
        }

        private string xmlFilePath;
        public string XmlFilePath
        {
            get { return xmlFilePath; }
            set
            {
                Set<string>(ref xmlFilePath, value);
            }
        }

        private double _offset;
        public double Offset
        {
            get { return _offset; }
            set { Set<double>(ref _offset, value); }
        }

        public string EndomondoTrainingStartTime
        {
            get;
            private set;
        }

        public string PptTrainingStartTime
        {
            get;
            private set;
        }


        public void Merge()
        {
            throw new NotImplementedException();
        }

        public void SaveToFile()
        {
            if (OnSaveFile != null)
                OnSaveFile(this, null);
            throw new NotImplementedException();
        }

        public event EventHandler<SaveFileEventArgs> OnSaveFile;
    }
}
