﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PptToEndomondo.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("PptToEndomondo.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ppt to Endomondo.
        /// </summary>
        public static string ApplicationTitle {
            get {
                return ResourceManager.GetString("ApplicationTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ....
        /// </summary>
        public static string BrowseButtonText {
            get {
                return ResourceManager.GetString("BrowseButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to endomondo.com.
        /// </summary>
        public static string Endomondo {
            get {
                return ResourceManager.GetString("Endomondo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Done.{0}New file save as {1}..
        /// </summary>
        public static string FinishMessage {
            get {
                return ResourceManager.GetString("FinishMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to First, select file from {0}.
        /// </summary>
        public static string FirstSelectFileLabel {
            get {
                return ResourceManager.GetString("FirstSelectFileLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to hour.
        /// </summary>
        public static string Hour {
            get {
                return ResourceManager.GetString("Hour", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to hours.
        /// </summary>
        public static string Hours {
            get {
                return ResourceManager.GetString("Hours", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Merge &amp; Save.
        /// </summary>
        public static string MergeButtonText {
            get {
                return ResourceManager.GetString("MergeButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Only {0} file|*.{0}.
        /// </summary>
        public static string OpenFileDialogFilter {
            get {
                return ResourceManager.GetString("OpenFileDialogFilter", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Select the file exported from {0}.
        /// </summary>
        public static string OpenFileDialogTitle {
            get {
                return ResourceManager.GetString("OpenFileDialogTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to polarpersonaltrainer.com.
        /// </summary>
        public static string Polarpersonaltrainer {
            get {
                return ResourceManager.GetString("Polarpersonaltrainer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to File from endomondo.com.
        /// </summary>
        public static string TcxFileLabel {
            get {
                return ResourceManager.GetString("TcxFileLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Offset is {0} {1}.
        /// </summary>
        public static string TimeOffsetDefaultOffsetLabel {
            get {
                return ResourceManager.GetString("TimeOffsetDefaultOffsetLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Endomondo training starts at {0}.
        /// </summary>
        public static string TimeOffsetEndomondoLabel {
            get {
                return ResourceManager.GetString("TimeOffsetEndomondoLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Time offset.
        /// </summary>
        public static string TimeOffsetLabel {
            get {
                return ResourceManager.GetString("TimeOffsetLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Polar training starts at {0}.
        /// </summary>
        public static string TimeOffsetPptLabel {
            get {
                return ResourceManager.GetString("TimeOffsetPptLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Both training should begin at about the same time.
        /// </summary>
        public static string TimeOffsetToolTip {
            get {
                return ResourceManager.GetString("TimeOffsetToolTip", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to File from polarpersonaltrainer.com.
        /// </summary>
        public static string XmlFileLabel {
            get {
                return ResourceManager.GetString("XmlFileLabel", resourceCulture);
            }
        }
    }
}
