﻿using Core;
using Core.Data.Endomondo.Gpx;
using Core.Data.Ppt.Xml;
using GalaSoft.MvvmLight;
using System;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using Core.Data;

namespace PptToEndomondo.Model
{
    public class ClientData : ObservableObject, IClientData
    {
        public ClientData()
        {
            gpxLoader = new Core.Data.Endomondo.Loader();
            polarXmlLoader = new Core.Data.Ppt.Loader();
            TimeFormat = "t";
        }

        private readonly string TimeFormat;

        private ILoaderAndSaver<gpx> gpxLoader;
        private ILoader<polarexercisedata> polarXmlLoader;
        private gpx endomondoGpxData;
        private polarexercisedata polarData;
        private DateTime endomondoTrainingStartTimeOriginal;
        private DateTime pptTrainingStartTimeOriginal;

        public event EventHandler<SaveFileEventArgs> OnSaveFile;


#region Properties

        public bool IsReady
        {
            get { return endomondoGpxData != null && polarData != null; }
        }

        private string gpxFilePath;
        public string GpxFilePath
        {
            get { return gpxFilePath; }
            set
            {
                Set<string>(ref gpxFilePath, value);
                endomondoGpxData = LoadFile<gpx>(gpxLoader, GpxFilePath);
                if (endomondoGpxData != null &&
                    endomondoGpxData.trk.trkseg != null &&
                    endomondoGpxData.trk.trkseg.Count > 1)
                {
                    endomondoTrainingStartTimeOriginal = endomondoGpxData.trk.trkseg.First().trkpt.First().time;
                    RaisePropertyChanged(() => EndomondoTrainingStartTime);
                    RaisePropertyChanged(() => IsReady);
                }
            }
        }

        private string xmlFilePath;
        public string XmlFilePath
        {
            get { return xmlFilePath; }
            set
            {
                Set<string>(ref xmlFilePath, value);
                polarData = LoadFile<polarexercisedata>(polarXmlLoader, XmlFilePath);
                if (polarData != null &&
                    polarData.calendaritems != null &&
                    polarData.calendaritems.exercise != null)
                {
                    pptTrainingStartTimeOriginal = Convert.ToDateTime(polarData.calendaritems.exercise.time);
                    RaisePropertyChanged(() => PptTrainingStartTime);
                    RaisePropertyChanged(() => IsReady);
                }
            }
        }

        private double offset;
        public double Offset
        {
            get
            {
                return offset;
            }
            set
            {
                Set(ref offset, value);
                RaisePropertyChanged(() => EndomondoTrainingStartTime);
            }
        }

        public string EndomondoTrainingStartTime
        {
            get
            {
                if (endomondoTrainingStartTimeOriginal == DateTime.MinValue)
                    return string.Format(Properties.Resources.FirstSelectFileLabel,
                        Properties.Resources.Endomondo);
                else
                {
                    return string.Format(Properties.Resources.TimeOffsetEndomondoLabel,
                        endomondoTrainingStartTimeOriginal.AddHours(Offset).ToString(TimeFormat));
                }
            }
        }

        public string PptTrainingStartTime
        {
            get
            {
                if (pptTrainingStartTimeOriginal == DateTime.MinValue)
                    return string.Format(Properties.Resources.FirstSelectFileLabel,
                        Properties.Resources.Polarpersonaltrainer);
                else
                {
                    return string.Format(Properties.Resources.TimeOffsetPptLabel,
                        pptTrainingStartTimeOriginal.ToString(TimeFormat));
                }
            }
        } 
        
#endregion //Properties

        private static T LoadFile<T>(ILoader<T> loader, string filePath) where T : class, new()
        {
            if (loader == null)
                throw new ArgumentNullException("loader");

            if (string.IsNullOrWhiteSpace(filePath) ||
                !File.Exists(filePath))
                return null;

            return loader.LoadDataFromFile(filePath);
        }

        public void Merge()
        {
            Merger.Merge(endomondoGpxData, polarData, Convert.ToInt32(Offset));
        }

        public void SaveToFile()
        {
            var fileName = FileUtils.GetFreeOutputFileName(GpxFilePath);
            gpxLoader.SaveDataToFile(endomondoGpxData, fileName);

            if (OnSaveFile != null)
                OnSaveFile(this, new SaveFileEventArgs(fileName));
        }
    }
}
