﻿using System;
using System.ComponentModel;
namespace PptToEndomondo.Model
{
    public interface IClientData : INotifyPropertyChanged
    {
        string GpxFilePath { get; set; }
        
        string EndomondoTrainingStartTime { get; }
        string PptTrainingStartTime { get; }

        bool IsReady { get; }
                
        double Offset { get; set; }
        
        string XmlFilePath { get; set; }

        void Merge();
        void SaveToFile();

        event EventHandler<SaveFileEventArgs> OnSaveFile;
    }
}
