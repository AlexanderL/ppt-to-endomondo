﻿using System;

namespace PptToEndomondo.Model
{
    public class SaveFileEventArgs : EventArgs
    {
        public SaveFileEventArgs(string fileName)
        {
            FileName = fileName;
        }
        public string FileName { get; set; }
    }
}
