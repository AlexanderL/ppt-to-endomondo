﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PptToEndomondo.Model
{
    internal struct FileExtensions
    {
        internal const string GPX = "gpx";
        internal const string XML = "xml";
    }
}
