﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Microsoft.Win32;
using PptToEndomondo.Model;
using System;
using System.Windows;
using System.Windows.Input;

namespace PptToEndomondo.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        public MainWindowViewModel(IClientData clientData)
        {
            ClientData = clientData;
            ClientData.PropertyChanged += ClientData_PropertyChanged;
            ClientData.OnSaveFile += ClientData_OnSaveFile;
        }

#region Properties

        public IClientData ClientData
        {
            get;
            private set;
        }

        public string OffsetLabel
        {
            get
            {
                return string.Format(Properties.Resources.TimeOffsetDefaultOffsetLabel,
                    ClientData.Offset,
                    System.Math.Abs(ClientData.Offset) == 1 ? Properties.Resources.Hour : Properties.Resources.Hours);
            }
        }

#endregion //Properties

#region BrowseTcxFileCommand

        private ICommand browseGpxFileCommand;
        public ICommand BrowseGpxFileCommand
        {
            get
            {
                return browseGpxFileCommand ??
                    (browseGpxFileCommand = new RelayCommand(BrowseGpxFileImpl));
            }
        }

        private void BrowseGpxFileImpl()
        {
            var fileName = ShowOpenFileDialogAndGetFileName(FileSources.EndomondoGpx);
            if (!string.IsNullOrWhiteSpace(fileName))
                ClientData.GpxFilePath = fileName;
        }

#endregion //BrowseTcxFileCommand

#region BrowseXmlFileCommand

        private ICommand browseXmlFileCommand;
        public ICommand BrowseXmlFileCommand
        {
            get
            {
                return browseXmlFileCommand ??
                    (browseXmlFileCommand = new RelayCommand(BrowseXmlFileImpl));
            }
        }

        private void BrowseXmlFileImpl()
        {
            var fileName = ShowOpenFileDialogAndGetFileName(FileSources.Polarpersonaltrainer);
            if (!string.IsNullOrWhiteSpace(fileName))
                ClientData.XmlFilePath = fileName;
        }

#endregion //BrowseXmlFileCommand

#region MergeCommand

        private ICommand mergeCommand;
        public ICommand MergeCommand
        {
            get
            {
                return mergeCommand ??
                    (mergeCommand = new RelayCommand(MergeImpl, CanMerge));
            }
        }

        private bool CanMerge()
        {
            return ClientData.IsReady;
        }

        private void MergeImpl()
        {
            ClientData.Merge();
            ClientData.SaveToFile();
        }

#endregion //MergeCommand

        private string ShowOpenFileDialogAndGetFileName(FileSources fileSource)
        {
            var ofd = new OpenFileDialog();
            SetOpenDialogTitleAndFilter(ofd, fileSource);
            ofd.CheckFileExists = true;
            ofd.Multiselect = false;
            if(ofd.ShowDialog().Value)
                return ofd.FileName;
            return null;
        }

        private void SetOpenDialogTitleAndFilter(OpenFileDialog openFileDialog, FileSources fileSource)
        {
            switch (fileSource)
            {
                case FileSources.EndomondoGpx:
                    openFileDialog.Title = string.Format(Properties.Resources.OpenFileDialogTitle,
                        Properties.Resources.Endomondo);
                    openFileDialog.Filter = string.Format(Properties.Resources.OpenFileDialogFilter,
                        FileExtensions.GPX);
                    break;
                case FileSources.Polarpersonaltrainer:
                    openFileDialog.Title = string.Format(Properties.Resources.OpenFileDialogTitle,
                        Properties.Resources.Polarpersonaltrainer);
                    openFileDialog.Filter = string.Format(Properties.Resources.OpenFileDialogFilter,
                        FileExtensions.XML);
                    break;
            }
        }

        void ClientData_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("Offset", System.StringComparison.OrdinalIgnoreCase))
                RaisePropertyChanged(() => OffsetLabel);
        }

        void ClientData_OnSaveFile(object sender, SaveFileEventArgs e)
        {
            MessageBox.Show(string.Format(
                Properties.Resources.FinishMessage,
                Environment.NewLine,
                e.FileName),
                Properties.Resources.ApplicationTitle,
                MessageBoxButton.OK,
                MessageBoxImage.Information);
        }
    }
}
